1. theme-a eguneratzeko fluxua

    1.1. repoa klonatu eta branch berri bat sortu

    1.2. aldaketa txikiren bat pusheatu

    1.3. merge bat egin developera

        1.3.1. erakutsi Jenkins lanak

        1.3.2. Igo developeko packages.json-en bertsioa

    1.4. merge bat egin developetik masterrera

        1.4.1. erakutsi Jenkins lanak

        1.4.2. erakutsi aldaketak clms-front-ean

        1.4.3. erakutsi npm repositorioa

        1.4.4. erakutsi clms-front-eko komitak eragindako docker irudi sorrera (latest)

2. Webgunearen release berri bat prestatu

    2.1. tageatu bertsio berri bat clms-front-en

         2.1.1. erakustsi Jenkins lanak

         2.1.2. erakutsi hub.docker.com-en sortutako irudi berria (tag)

         2.1.3. erakutsi clms-front-en eragiten den package.json-eko bertsio eguneraketa

         2.1.4. erakutsi rancher catalogean eragindako aldaketa

         2.1.5. erakutsi rancher-ean update aukera

3. Denbora baldin badago python paketeen fluxua erakutsi  